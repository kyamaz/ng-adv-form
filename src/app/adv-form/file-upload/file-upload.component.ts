import { HttpClient, HttpEventType } from "@angular/common/http";
import { Component, Input } from "@angular/core";
import {
  AbstractControl,
  ControlValueAccessor,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  ValidationErrors,
  Validator,
} from "@angular/forms";
import { Observable, of } from "rxjs";
import { catchError, delay, finalize, tap } from "rxjs/operators";

@Component({
  selector: "app-file-upload",
  templateUrl: "./file-upload.component.html",
  styleUrls: ["./file-upload.component.css"],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: FileUploadComponent,
    },
    {
      provide: NG_VALIDATORS,
      multi: true,
      useExisting: FileUploadComponent,
    },
  ],
})
export class FileUploadComponent implements ControlValueAccessor, Validator {
  @Input()
  requiredFileType: string;

  fileName: string = "";
  uploadProgress: number;

  onChange = (fileName) => {};
  onTouch = () => {};

  onValidatorChange = () => {};
  disabled = false;
  fileUploadSuccess = false;
  fileUploadFailed = false;

  constructor(private httpClient: HttpClient) {}

  onFileSelected(ev) {
    const file: File = ev.target.files[0];
    if (file) {
      this.fileName = file.name;
      const forrmData = new FormData();
      forrmData.append("thumbnail", file);

      const moke$ = new Observable((obsever) => {
        obsever.next("uploaded");
        obsever.complete();
      });
      this.httpClient
        .post("someurl", forrmData, {
          reportProgress: true,
          observe: "events",
        })
        .pipe(
          catchError((err) => {
            this.fileUploadFailed = true;
            return of(err);
          }),
          finalize(() => {
            this.uploadProgress = null;
          }),
          tap((event) => {
            if (event.type === HttpEventType.UploadProgress) {
              this.uploadProgress = Math.round(
                100 * (event.loaded / event.total)
              );
            }

            if (event.type === HttpEventType.Response) {
              this.onChange(this.fileName);
              this.fileUploadSuccess = true;
              this.onValidatorChange();
            }
          })
        );
      moke$
        .pipe(
          delay(2000),
          tap((_) => {
            this.onChange(this.fileName);
            this.fileUploadSuccess = true;
            this.onValidatorChange();
          })
        )
        .subscribe();
    }
  }
  // ctrl value accessor

  writeValue(value: string) {
    this.fileName = value;
  }
  registerOnChange(onChange) {
    this.onChange = onChange;
  }

  registerOnTouched(onTouch) {
    this.onTouch = onTouch;
  }
  setDisabledState(isDisabled) {
    this.disabled = isDisabled;
  }

  onClick(fileEl: HTMLInputElement) {
    fileEl.click();
    this.onTouch();
  }

  //Validatoars
  validate(ctrl: AbstractControl): ValidationErrors | null {
    if (this.fileUploadSuccess) {
      return null;
    }

    let errs: any = {
      requiredFileType: this.requiredFileType,
    };
    if (this.fileUploadFailed) {
      errs.fileUploadFailed = this.fileUploadFailed;
    }
    return errs;
  }

  registerOnValidatorChange(onValidatorChange) {
    this.onValidatorChange = onValidatorChange;
  }
}
