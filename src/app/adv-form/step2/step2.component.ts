import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  ValidatorFn,
  Validators,
} from "@angular/forms";
import { tap } from "rxjs/operators";

function dateRangeValidator(): ValidatorFn {
  return (form: FormGroup): Validators | null => {
    const start: Date = form.get("promoStartAt").value;
    const end: Date = form.get("promoEndAt").value;

    if (start && end) {
      const isDateRange = end.getTime() - start.getTime() > 0;

      return isDateRange ? null : { rangeInvalid: true };
    }

    return { rangeInvalid: true };
  };
}

@Component({
  selector: "app-step2",
  templateUrl: "./step2.component.html",
  styleUrls: ["./step2.component.css"],
})
export class Step2Component implements OnInit {
  form = this.formBuilder.group(
    {
      type: ["free", [Validators.required]],
      price: [
        null,
        [
          Validators.required,
          Validators.min(1),
          Validators.max(999),
          Validators.pattern("[0-9]+"),
        ],
      ],
      promoStartAt: [null],
      promoEndAt: [null],
      thumbnail: [null],
    },
    {
      validators: [dateRangeValidator()],
      // updateOn: "blur",
    }
  );

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.form
      .get("type")
      .valueChanges.pipe(
        tap((val) => {
          const priceFiel = this.form.get("price");
          if (val === "free" && priceFiel.enabled) {
            priceFiel.disable({ emitEvent: false });
          } else if (val === "premium" && priceFiel.disabled) {
            priceFiel.enable({ emitEvent: false });
          }
        })
      )
      .subscribe();

    this.form.statusChanges.subscribe((f) => console.log(this.form));
  }
}
