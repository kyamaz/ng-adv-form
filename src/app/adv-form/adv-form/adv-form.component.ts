import { STEPPER_GLOBAL_OPTIONS } from "@angular/cdk/stepper";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-adv-form",
  templateUrl: "./adv-form.component.html",
  styleUrls: ["./adv-form.component.css"],
  providers: [
    {
      provide: STEPPER_GLOBAL_OPTIONS,
      useValue: { showError: true },
    },
  ],
})
export class AdvFormComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
