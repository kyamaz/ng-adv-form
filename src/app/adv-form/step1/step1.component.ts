import { Component, OnInit } from "@angular/core";
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  ValidationErrors,
  Validators,
} from "@angular/forms";
import { Observable } from "rxjs";
import { delay, filter, tap } from "rxjs/operators";
interface CourseCategory {
  code: string;
  description: string;
}

function asnycCheck(title: string): Observable<null | ValidationErrors> {
  const v =
    title === "unique"
      ? {
          titleExists: true,
        }
      : null;

  return new Observable((observer) => {
    observer.next(v);
    observer.complete();
  });
}

function asyncTitleValidation() {
  return (control: AbstractControl) => {
    return asnycCheck(control.value).pipe(delay(1000));
  };
}

@Component({
  selector: "app-step1",
  templateUrl: "./step1.component.html",
  styleUrls: ["./step1.component.css"],
})
export class Step1Component implements OnInit {
  form: FormGroup = this.formBuilder.group({
    title: [
      "",

      {
        validators: [
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(60),
        ],
        asyncValidators: [asyncTitleValidation.call(this)],
        updateOn: "blur",
      },
      ,
    ],
    releaseDate: [new Date(), [Validators.required]],
    downloadAllowed: [false, [Validators.requiredTrue]],
    description: ["", [Validators.required, Validators.min(5)]],
    category: ["", [Validators.required]],
    address: [null, [Validators.required]],
  });
  courseCategories$: Observable<CourseCategory[]>;

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit(): void {
    if (localStorage.getItem("step_1")) {
      this.form.setValue(JSON.parse(localStorage.getItem("step_1")));
    }
    this.courseCategories$ = new Observable((observer) => {
      observer.next([
        {
          code: "0",
          description: "beginner",
        },
        {
          code: "1",
          description: "intermediate",
        },
      ]);
      observer.complete();
    });

    this.form.valueChanges
      .pipe(
        filter(() => this.form.valid),
        tap((f) => localStorage.setItem("step_1", JSON.stringify(f)))
      )
      .subscribe();
  }
}
