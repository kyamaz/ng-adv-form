import { Component, OnDestroy } from "@angular/core";
import {
  ControlValueAccessor,
  FormBuilder,
  NG_VALUE_ACCESSOR,
  Validators,
} from "@angular/forms";
import { Subscription } from "rxjs";
import { tap } from "rxjs/operators";

@Component({
  selector: "app-address-form",
  templateUrl: "./address-form.component.html",
  styleUrls: ["./address-form.component.css"],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: AddressFormComponent,
    },
  ],
})
export class AddressFormComponent implements ControlValueAccessor, OnDestroy {
  form = this.formBuilder.group({
    address1: [null, [Validators.required]],
    address2: [null, []],
    zip: [null, [Validators.required]],
    city: [null, [Validators.required]],
    country: [null, [Validators.required]],
  });
  onChangeSub: Subscription;

  constructor(private formBuilder: FormBuilder) {}

  ngOnDestroy() {
    this.onChangeSub.unsubscribe();
  }
  onTouched = () => {};

  writeValue(value) {
    if (value) {
      this.form.setValue(value);
    }
  }
  registerOnChange(onChange) {
    this.onChangeSub = this.form.valueChanges.pipe(tap(onChange)).subscribe();
  }
  registerOnTouched(onTouched) {
    this.onTouched = onTouched;
  }
  setDisabledState(disable) {
    if (disable) {
      this.form.disable();
      return;
    }
    this.form.enable();
  }
}
