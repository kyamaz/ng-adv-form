import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent {
  title = "adv";
  isLoginActive = true;

  setIsLoginActive(next) {
    this.isLoginActive = next;
  }
}
