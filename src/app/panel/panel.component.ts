import {
  Component,
  ContentChildren,
  Input,
  OnInit,
  QueryList,
  TemplateRef,
} from "@angular/core";
import { TabComponent } from "./../tab/tab.component";

@Component({
  selector: "app-panel",
  templateUrl: "./panel.component.html",
  styleUrls: ["./panel.component.css"],
})
export class PanelComponent implements OnInit {
  @ContentChildren(TabComponent)
  tabs: QueryList<TabComponent>;

  @Input()
  headerTemplate: TemplateRef<any>;

  get tabsContext() {
    return {
      tab: this.tabs,
    };
  }
  constructor() {}

  ngOnInit(): void {}
  ngAfterContentInit() {
    const selTab = this.tabs.find((t) => t.selected);
    if (!selTab) {
      this.tabs.first.selected = true;
    }
  }

  selectTab(t: TabComponent) {
    this.tabs.forEach((el) => (el.selected = false));

    t.selected = true;
  }
}
