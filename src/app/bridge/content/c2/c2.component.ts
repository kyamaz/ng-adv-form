import { Component, OnInit } from "@angular/core";
import { ContentInterface } from "./../Content.interface";
import { CONTEN_TOKEN } from "./../content.tokent";

@Component({
  selector: "app-c2",
  templateUrl: "./c2.component.html",
  styleUrls: ["./c2.component.css"],
  providers: [
    {
      provide: CONTEN_TOKEN,
      useExisting: C2Component,
    },
  ],
})
export class C2Component implements OnInit, ContentInterface {
  constructor() {}

  ngOnInit(): void {}

  refresh() {
    console.log("c2");
  }
}
