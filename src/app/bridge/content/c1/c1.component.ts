import { Component, OnInit } from "@angular/core";
import { CONTEN_TOKEN } from "../content.tokent";
import { ContentInterface } from "./../Content.interface";

@Component({
  selector: "app-c1",
  templateUrl: "./c1.component.html",
  styleUrls: ["./c1.component.css"],
  providers: [
    {
      provide: CONTEN_TOKEN,
      useExisting: C1Component,
    },
  ],
})
export class C1Component implements OnInit, ContentInterface {
  constructor() {}

  ngOnInit(): void {}
  refresh() {
    console.log("c1");
  }
}
