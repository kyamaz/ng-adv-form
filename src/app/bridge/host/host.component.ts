import { Component, ContentChild, OnInit } from "@angular/core";
import { ContentInterface } from "./../content/Content.interface";
import { CONTEN_TOKEN } from "./../content/content.tokent";

@Component({
  selector: "app-host",
  templateUrl: "./host.component.html",
  styleUrls: ["./host.component.css"],
})
export class HostComponent implements OnInit {
  constructor() {}
  @ContentChild(CONTEN_TOKEN, { static: true })
  content: ContentInterface;

  ngOnInit(): void {}

  refresh() {
    this.content.refresh();
  }
}
