export const SPECIAL_CHAR = [" ", "/", "(", ")", "-", "+", "/"];
export const TAB = 9;
export const LEFT_ARROW = 37;
export const RIGHT_ARROW = 39;
export const BACK_SPACE = 8;
export const DELETE = 46;
