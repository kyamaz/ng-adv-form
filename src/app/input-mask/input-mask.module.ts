import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormComponent } from "./form/form.component";
import { MaskDirective } from './mask.directive';

@NgModule({
  declarations: [FormComponent, MaskDirective],
  imports: [CommonModule],
  exports: [FormComponent],
})
export class InputMaskModule {}
