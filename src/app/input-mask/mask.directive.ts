import {
  Directive,
  ElementRef,
  HostListener,
  Input,
  OnInit,
} from "@angular/core";
import * as lodash from "lodash";
import {
  BACK_SPACE,
  DELETE,
  LEFT_ARROW,
  RIGHT_ARROW,
  SPECIAL_CHAR,
  TAB,
} from "./const";
function overrideCurrentValue(key, selectionStart, value) {
  return value.slice(0, selectionStart) + key + value.slice(selectionStart + 1);
}

function digitValidator(char) {
  return /[0-9]{1}/.test(char);
}
function charCapValidaor(char) {
  return /[A-Z]{1}/.test(char);
}
function charSmallValidaor(char) {
  return /[a-z]{1}/.test(char);
}
function numberRabgeValidator(maxValue, char) {
  return digitValidator(char) && parseInt(char) <= maxValue;
}
function anyVal(char) {
  return true;
}
function neverVal(char) {
  return false;
}
const maskDigitValidator = {
  "9": numberRabgeValidator.bind(undefined, 9),
  A: charCapValidaor.bind(undefined),
  a: charSmallValidaor.bind(undefined),
  "*": anyVal,
};
function digitValidation() {}
@Directive({
  selector: "[appMask]",
})
export class MaskDirective implements OnInit {
  @Input()
  appMask = "";

  input: HTMLInputElement;

  fullFieldSelected = false;
  constructor(el: ElementRef) {
    this.input = el.nativeElement;
  }
  ngOnInit() {
    this.input.value = this.buildPlaceHolder();
  }
  @HostListener("keydown", ["$event", "$event.keyCode"])
  onKeyDown($event: KeyboardEvent, keyCode) {
    if ($event.metaKey || $event.ctrlKey) {
      return;
    }
    if (keyCode !== TAB) {
      $event.preventDefault();
    }
    const cursorPos = this.input.selectionStart;

    if (this.fullFieldSelected) {
      this.input.value = this.buildPlaceHolder();
      const firstPlaceHolder = lodash.findIndex(
        this.input.value,
        (char) => char === "_"
      );
      this.input.setSelectionRange(firstPlaceHolder, firstPlaceHolder);
    }

    switch (keyCode) {
      case LEFT_ARROW: {
        this.handleLeftArrow(cursorPos);
        return;
      }

      case RIGHT_ARROW: {
        this.handleRighArrow(cursorPos);

        return;
      }
      case BACK_SPACE: {
        this.handleBackspace(cursorPos);

        return;
      }
      case DELETE: {
        this.handleDelete(cursorPos);

        return;
      }
      default:
        break;
    }

    const maskDigit = this.appMask.charAt(cursorPos);
    const digitValidate = maskDigitValidator[maskDigit] || neverVal;
    if (digitValidate($event.key)) {
      this.input.value = overrideCurrentValue(
        $event.key,
        cursorPos,
        this.input.value
      );
      this.handleRighArrow(cursorPos);
    }
  }

  @HostListener("select", ["$event"])
  onSelect($event: UIEvent) {
    this.fullFieldSelected =
      this.input.selectionStart === 0 &&
      this.input.selectionEnd === this.input.value.length;
  }

  handleLeftArrow(cursorPos) {
    const valueBefCursor = this.input.value.slice(0, cursorPos);
    const prevPos = lodash.findLastIndex(
      valueBefCursor,
      (char) => !SPECIAL_CHAR.find((el) => el === char)
    );
    if ((prevPos) => 0) {
      this.input.setSelectionRange(prevPos, prevPos);
    }
  }

  handleRighArrow(cursorPos) {
    const valueAftCursor = this.input.value.slice(cursorPos + 1);
    const next = lodash.findIndex(
      valueAftCursor,
      (char) => !SPECIAL_CHAR.find((el) => el === char)
    );
    if (next >= 0) {
      this.input.setSelectionRange(cursorPos + next + 1, cursorPos + next + 1);
    }
  }
  handleBackspace(cursor) {
    const valueBefCursor = this.input.value.slice(0, cursor);
    const prevPos = lodash.findLastIndex(
      valueBefCursor,
      (char) => !SPECIAL_CHAR.find((el) => el === char)
    );
    if (prevPos > 0) {
      this.input.value = overrideCurrentValue("_", prevPos, this.input.value);
      this.input.setSelectionRange(prevPos, prevPos);
    }
  }
  handleDelete(cursor) {
    this.input.value = overrideCurrentValue("_", cursor, this.input.value);
    this.input.setSelectionRange(cursor, cursor);
  }
  buildPlaceHolder() {
    const chars = this.appMask.split("");

    return chars.reduce((acc, curr) => {
      return (acc += SPECIAL_CHAR.find((el) => el === curr) ? curr : "_");
    }, "");
  }
}
