import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AdvFormModule } from "./adv-form/adv-form.module";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { InputMaskModule } from "./input-mask/input-mask.module";
import { ModalModule } from "./modal/modal.module";
import { PanelComponent } from "./panel/panel.component";
import { TabComponent } from "./tab/tab.component";
import { HostComponent } from './bridge/host/host.component';
import { C1Component } from './bridge/content/c1/c1.component';
import { C2Component } from './bridge/content/c2/c2.component';

@NgModule({
  declarations: [AppComponent, PanelComponent, TabComponent, HostComponent, C1Component, C2Component],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ModalModule.forRoot(),
    InputMaskModule,
    AdvFormModule,
    BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
