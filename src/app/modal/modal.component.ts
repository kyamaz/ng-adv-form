import { Component, Input, OnInit, TemplateRef } from "@angular/core";
import { EventManager } from "@angular/platform-browser";
import { ModalService } from "./modal.service";

@Component({
  selector: "app-modal",
  templateUrl: "./modal.component.html",
  styleUrls: ["./modal.component.css"],
})
export class ModalComponent implements OnInit {
  @Input()
  body: TemplateRef<any>;

  @Input()
  hideOnEscape = false;

  @Input()
  context: any;

  @Input()
  hideOnClickOutside = false;
  constructor(
    private modalService: ModalService,
    private eventManager: EventManager
  ) {}

  ngOnInit(): void {
    this.eventManager.addGlobalEventListener("window", "keyup.esc", () => {
      if (this.hideOnEscape) {
        this.closeModal();
      }
    });
  }
  closeModal() {
    this.modalService.close();
  }
  cancelClick(ev: KeyboardEvent) {
    ev.preventDefault();
    ev.stopPropagation();
  }

  onClickOutsideModal() {
    if (this.hideOnClickOutside) {
      this.closeModal();
    }
  }
}
