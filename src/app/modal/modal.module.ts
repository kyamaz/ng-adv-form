import { CommonModule } from "@angular/common";
import { ModuleWithProviders, NgModule } from "@angular/core";
import { ModalComponent } from "./modal.component";
import { OpenOnClickDirective } from "./open-on-click.directive";

@NgModule({
  declarations: [ModalComponent, OpenOnClickDirective],
  imports: [CommonModule],
  exports: [ModalComponent, OpenOnClickDirective],
})
export class ModalModule {
  constructor() {}
  static forRoot(): ModuleWithProviders<ModalModule> {
    return {
      ngModule: ModalModule,
      providers: [],
    };
  }
}
