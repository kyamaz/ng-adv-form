import {
  Directive,
  Input,
  OnInit,
  TemplateRef,
  ViewContainerRef,
} from "@angular/core";
import { tap } from "rxjs/operators";
import { ModalService } from "./modal.service";
@Directive({
  selector: "[appOpenOnClick]",
})
export class OpenOnClickDirective implements OnInit {
  elements: HTMLBaseElement[] = [];
  @Input()
  set appOpenOnClick(els: HTMLBaseElement[]) {
    this.elements = els;
    this.elements.forEach((el) => {
      el.addEventListener("click", this.clickHandler.bind(this));
    });
  }

  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef,
    private modalService: ModalService
  ) {}
  ngOnInit() {
    this.modalService.close$
      .pipe(tap((_) => this.viewContainer.clear()))
      .subscribe();
  }
  ngOnDestroy() {
    this.elements.forEach((el) =>
      el.removeEventListener("click", this.clickHandler)
    );
  }

  clickHandler() {
    this.viewContainer.clear();
    this.viewContainer.createEmbeddedView(this.templateRef);
  }
}
